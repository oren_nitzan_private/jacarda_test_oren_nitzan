﻿namespace Jacada
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnIE = new System.Windows.Forms.Button();
            this.btnCalc = new System.Windows.Forms.Button();
            this.grpRadOppType = new System.Windows.Forms.GroupBox();
            this.radEvent = new System.Windows.Forms.RadioButton();
            this.radSearch = new System.Windows.Forms.RadioButton();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.txtOppText = new System.Windows.Forms.TextBox();
            this.btnRunCommand = new System.Windows.Forms.Button();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.grpRadOppType.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnIE
            // 
            this.btnIE.Location = new System.Drawing.Point(12, 12);
            this.btnIE.Name = "btnIE";
            this.btnIE.Size = new System.Drawing.Size(153, 46);
            this.btnIE.TabIndex = 0;
            this.btnIE.Text = "IE";
            this.btnIE.UseVisualStyleBackColor = true;
            this.btnIE.Click += new System.EventHandler(this.btnIE_Click);
            // 
            // btnCalc
            // 
            this.btnCalc.Location = new System.Drawing.Point(182, 12);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(153, 46);
            this.btnCalc.TabIndex = 1;
            this.btnCalc.Text = "Calc";
            this.btnCalc.UseVisualStyleBackColor = true;
            this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
            // 
            // grpRadOppType
            // 
            this.grpRadOppType.Controls.Add(this.radEvent);
            this.grpRadOppType.Controls.Add(this.radSearch);
            this.grpRadOppType.Location = new System.Drawing.Point(356, 12);
            this.grpRadOppType.Name = "grpRadOppType";
            this.grpRadOppType.Size = new System.Drawing.Size(222, 46);
            this.grpRadOppType.TabIndex = 2;
            this.grpRadOppType.TabStop = false;
            this.grpRadOppType.Text = "Opp Type";
            // 
            // radEvent
            // 
            this.radEvent.AutoSize = true;
            this.radEvent.Location = new System.Drawing.Point(101, 19);
            this.radEvent.Name = "radEvent";
            this.radEvent.Size = new System.Drawing.Size(68, 17);
            this.radEvent.TabIndex = 1;
            this.radEvent.Text = "By Event";
            this.radEvent.UseVisualStyleBackColor = true;
            // 
            // radSearch
            // 
            this.radSearch.AutoSize = true;
            this.radSearch.Checked = true;
            this.radSearch.Location = new System.Drawing.Point(6, 19);
            this.radSearch.Name = "radSearch";
            this.radSearch.Size = new System.Drawing.Size(74, 17);
            this.radSearch.TabIndex = 0;
            this.radSearch.TabStop = true;
            this.radSearch.Text = "By Search";
            this.radSearch.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(430, 249);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(8, 4);
            this.listBox1.TabIndex = 3;
            // 
            // txtOppText
            // 
            this.txtOppText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.txtOppText.Location = new System.Drawing.Point(13, 80);
            this.txtOppText.Name = "txtOppText";
            this.txtOppText.Size = new System.Drawing.Size(433, 26);
            this.txtOppText.TabIndex = 5;
            // 
            // btnRunCommand
            // 
            this.btnRunCommand.Location = new System.Drawing.Point(457, 64);
            this.btnRunCommand.Name = "btnRunCommand";
            this.btnRunCommand.Size = new System.Drawing.Size(108, 60);
            this.btnRunCommand.TabIndex = 6;
            this.btnRunCommand.Text = "Run Command";
            this.btnRunCommand.UseVisualStyleBackColor = true;
            this.btnRunCommand.Click += new System.EventHandler(this.btnRunCommand_Click);
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtData.Location = new System.Drawing.Point(13, 144);
            this.txtData.Multiline = true;
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(565, 233);
            this.txtData.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Process Data:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 415);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.btnRunCommand);
            this.Controls.Add(this.txtOppText);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.grpRadOppType);
            this.Controls.Add(this.btnCalc);
            this.Controls.Add(this.btnIE);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "Jcada Test";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.grpRadOppType.ResumeLayout(false);
            this.grpRadOppType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIE;
        private System.Windows.Forms.Button btnCalc;
        private System.Windows.Forms.GroupBox grpRadOppType;
        private System.Windows.Forms.RadioButton radEvent;
        private System.Windows.Forms.RadioButton radSearch;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox txtOppText;
        private System.Windows.Forms.Button btnRunCommand;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label1;
    }
}

