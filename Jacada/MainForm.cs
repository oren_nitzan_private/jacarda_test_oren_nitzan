﻿using SHDocVw;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Automation;
using System.Windows.Forms;

namespace Jacada
{
    public partial class MainForm : Form
    {
        // See: http://blog.functionalfun.net/2009/06/introduction-to-ui-automation-with.html
        // See: https://stackoverflow.com/questions/23850176/c-sharp-system-windows-automation-get-element-text
        // See: http://blog.wibeck.org/2008/11/fun-with-uiautomation-and-calc/
        // See: https://stackoverflow.com/questions/637819/find-window-height-width
        // See: https://docs.microsoft.com/en-us/dotnet/api/system.windows.automation.windowpattern.windowopenedevent?view=netframework-4.7.1
        // See: https://social.msdn.microsoft.com/Forums/sqlserver/en-US/c2aeec9b-b752-4d99-8722-77bf68486df3/ie-web-ui-automation-using-c?forum=ieextensiondevelopment

        #region Construction And Parameters ...

        // Current running process
        private Process runningProcess = null;
        private AutomationElement rootAutomationElement;
        //private AutomationElement rootCalcFrameAutomationElement;
        private ProcessType processType = ProcessType.None;
        private List<KeyData> calcKeysData = null;
        public MainForm()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(0, 0);
        }


        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {
            calcKeysData = Util.GetCalculatorSettings();
        }

        #region Calculator ...

        private void btnCalc_Click(object sender, EventArgs e)
        {
            // Kill previous process and reset root element to null.
            ResetResources();
            processType = ProcessType.Calc;

            // Start and get a hold of the calc process.
            runningProcess = StartCalcProcess();

            // Not sure!! registration by event needs not null target to register upon. But root came out as calcFrame and it looks like the same as the whole calculator??!!
            rootAutomationElement = AutomationElement.FromHandle(runningProcess.MainWindowHandle);

            // Initialize Automation object on search mode.
            if (radSearch.Checked)
            {
                InitiateRootAutomationElement();
            }
            else
            {
                RegisterForAutomationEvents();
            }

        }

        private Process StartCalcProcess()
        {
            Process process = new Process();
            process.StartInfo.FileName = "calc.exe";
            process.Start();
            // Delay a bit to allow the calculator to filly initialize. (As I saw in one of the articles)
            Thread.Sleep(1000);

            return process;
        }

        private void RunCalculatorAutomations()
        {
            string command = txtOppText.Text;
            if (string.IsNullOrEmpty(command))
            {
                MessageBox.Show("Please Enter a calculator command and try again. Example: 1+2=");
                return;
            }

            char[] calcChars = command.ToCharArray();

            // Iterate through the command characters
            foreach (char c in calcChars)
            {
                if (c == ' ')
                {
                    continue;
                }

                int ms = c.ToString() == "=" ? 500 : 150;
                Thread.Sleep(ms);


                // Get the calculator command name;
                string commandName = calcKeysData.Where(cr => cr.Key == c.ToString()).Select(s => s.Usage).FirstOrDefault();

                // Search for the element
                AutomationElement btnElm = GetCalculatorButtonBySearch(commandName, true);

                // Try to invoke it
                try
                {
                    // TODO: Interesting!! after a number button has been invoked once. The next time it is called (like in 1+1) it crushes here. The button is not clicked and the number does not shows on the calculator, although the result is ok because the calculator takes the numbers from memory (i guess). 
                    InvokePattern invPattern = btnElm.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                    invPattern.Invoke();
                }
                catch (Exception ex)
                {
                    string msg = string.Format("Error while executing command {0}. Error message: {1}", c, ex.Message);
                    MessageBox.Show(msg);
                }

            }

            #region One option i didn't use ...

            // This 'First Method' works but i chose to use the next one ('Second Method'). It is almost the same but more specific 
            //AutomationElementCollection elems = rootAutomationElement.FindAll(TreeScope.Descendants, PropertyCondition.TrueCondition);
            //AutomationElement resElement = elems[4]; // I ui spyed it.
            //string res = resElement.GetText();
            //txtOppText.Text += res;

            #endregion

            // Second Method
            AutomationElementCollection elemsLable = rootAutomationElement.GetElements(ControlType.Text);
            string calcTextResult = elemsLable[2].GetText();
            // Just to be sure. Both text elements 2 and 3 (index) holds the result.
            if (string.IsNullOrEmpty(calcTextResult))
            {
                calcTextResult = elemsLable[3].GetText();
            }

            txtOppText.Text = command + " " + calcTextResult;


            // Minimize
            // TODO: HideCalcWindow
            //HideCalcWindow();
        }

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool GetWindowRect(IntPtr hWnd, out Rectangle lpRect);

        private void ReportProcessData()
        {
            txtData.Text = string.Empty;

            string className = rootAutomationElement.GetCurrentPropertyValue(AutomationElement.ClassNameProperty) as string;
            string handle = runningProcess.MainWindowHandle.ToString();

            string title = "Unknown";

            if (processType == ProcessType.Calc)
            {
                AutomationElement titleElm = rootAutomationElement.GetElements(ControlType.TitleBar)[0];
                title = titleElm.Current.Name;
            }
            else if (processType == ProcessType.IE)
            {
                title = "Get from url maybe?? Or from html. Because there is no frame";
            }

            Rectangle rect = new Rectangle();
            GetWindowRect(runningProcess.MainWindowHandle, out rect);
            int calcWidth = rect.Width - rect.X;
            int calcHeight = rect.Height - rect.Y;

            txtData.Text += string.Format("Class Name - {0}{1}", className, Environment.NewLine);
            txtData.Text += string.Format("Handle - {0}{1}", handle, Environment.NewLine);
            txtData.Text += string.Format("Title - {0}{1}", title, Environment.NewLine);
            txtData.Text += string.Format("Width - {0}{1}", calcWidth.ToString(), Environment.NewLine);
            txtData.Text += string.Format("Height - {0}{1}", calcHeight.ToString(), Environment.NewLine);
            txtData.Text += string.Format("ProcessId - {0}{1}", runningProcess.Id.ToString(), Environment.NewLine);

        }

        private void HideCalcWindow()
        {

            AutomationElement min = GetCalculatorButtonBySearch("min", true);
            InvokePattern invPattern = min.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
            invPattern.Invoke();
        }

        private void InitiateRootAutomationElement()
        {
            // Theses two lines are give the same result (I believe, and for now it looks like it true).
            //rootAutomationElement = AutomationElement.RootElement.FindChildByProcessId(runningProcess.Id);
            rootAutomationElement = AutomationElement.FromHandle(runningProcess.MainWindowHandle);
            ReportProcessData();
        }

        ///--------------------------------------------------------------------
        /// <summary>
        /// Register for events of interest.
        /// </summary>
        /// <param name="targetControl">
        /// The automation element of interest.
        /// </param>
        ///--------------------------------------------------------------------
        private void RegisterForAutomationEvents()
        {
            AutomationEventHandler eventHandler = new AutomationEventHandler(OnWindowOpen);
            Automation.AddAutomationEventHandler(WindowPattern.WindowOpenedEvent, rootAutomationElement, TreeScope.Element, eventHandler);
            //Automation.AddAutomationEventHandler(WindowPattern.WindowClosedEvent, rootAutomationElement, TreeScope.Element, eventHandler);
        }

        ///--------------------------------------------------------------------
        /// <summary>
        /// AutomationEventHandler delegate.
        /// </summary>
        /// <param name="src">Object that raised the event.</param>
        /// <param name="e">Event arguments.</param>
        ///--------------------------------------------------------------------
        private void OnWindowOpen(object src, AutomationEventArgs e)
        {
            try
            {
                rootAutomationElement = src as AutomationElement;
            }
            catch (ElementNotAvailableException ex)
            {
                MessageBox.Show(string.Format("Could not handle opening . Error message: {0}", ex.Message));
                return;
            }

            if (e.EventId == WindowPattern.WindowOpenedEvent)
            {
                ReportProcessData();
                return;
            }
        }


        private AutomationElement GetCalculatorButtonBySearch(string name, bool recursive)
        {
            PropertyCondition condName = new PropertyCondition(AutomationElement.NameProperty, name);
            return rootAutomationElement.FindFirst(recursive ? TreeScope.Descendants : TreeScope.Children, condName);
        }

        #endregion

        #region IE ...

        private void btnIE_Click(object sender, EventArgs e)
        {
            // Kill previous process and reset root element to null.
            ResetResources();
            processType = ProcessType.IE;

            // Start and get a hold of the calc process.
            runningProcess = StartIEProcess();

            // Initialize Automation object
            try
            {
                rootAutomationElement = AutomationElement.FromHandle(runningProcess.MainWindowHandle);
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error while resetting or opening IE. Error message: {0}", ex.Message));
                return;
            }
            //// I get Process exited. maybe this can help
            //RegisterForAutomationEvents();
            ReportProcessData();
        }

        private Process StartIEProcess()
        {
            Process process = new Process();
            process.StartInfo.FileName = "iexplore.exe";
            process.StartInfo.Arguments = "-k https://www.jacada.com/";
            process.Start();

            // Delay a bit to allow the calculator to filly initialize. (As I saw in one of the articles)
            Thread.Sleep(1000);
            
            return process;
        }

        private void RunIEAutomations()
        {
            string command = txtOppText.Text;
            if (string.IsNullOrEmpty(command))
            {
                MessageBox.Show("Please Enter a url command and try again.");
                return;
            }

            InternetExplorer ie = null;
            ShellWindows allBrowsers = new ShellWindows();
            
                foreach (InternetExplorer item in allBrowsers)
                {
                    if (item.HWND == (int)runningProcess.MainWindowHandle)
                    {
                        ie = item;
                    }
                }

            ie.Navigate(command);
        }


        #endregion



        private void btnRunCommand_Click(object sender, EventArgs e)
        {
            switch (processType)
            {
                case ProcessType.Calc:
                    RunCalculatorAutomations();
                    break;
                case ProcessType.IE:
                    RunIEAutomations();
                    break;
                case ProcessType.None:
                default:
                    break;
            }
        }


        #region Closing And Releasing ...

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ResetResources();
        }

        private void ResetResources()
        {
            if (null != runningProcess)
            {
                //runningProcess.Kill();
                try
                {
                    runningProcess.CloseMainWindow();
                    runningProcess.Close();
                }
                catch (Exception)
                {
                    // TODO: Handle IE Reopen exception
                    // This is only for IE exception No time to handle it now.
                }
                runningProcess = null;
                runningProcess = null;
                txtOppText.Text = string.Empty;
                txtData.Text = string.Empty;
            }
            rootAutomationElement = null;
            processType = ProcessType.None;
        }


        #endregion

    }
}
