﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace Jacada
{

    public enum ProcessType
    {
        None,
        Calc,
        IE
    }

    public static class Util
    {

        public static List<KeyData> GetCalculatorSettings()
        {
            List<KeyData> buttonsData = new List<KeyData>();
            for (int i = 0; i <= 9; i++)
            {
                buttonsData.Add(new KeyData() { Key = i.ToString(), Usage = i.ToString() });
            }
            buttonsData.Add(new KeyData() { Key = ".", Usage = "Decimal separator" });
            buttonsData.Add(new KeyData() { Key = "+", Usage = "Add" });
            buttonsData.Add(new KeyData() { Key = "-", Usage = "Subtract" });
            buttonsData.Add(new KeyData() { Key = "*", Usage = "Multiply" });
            buttonsData.Add(new KeyData() { Key = "/", Usage = "Divide" });
            buttonsData.Add(new KeyData() { Key = "=", Usage = "Equals" });
            

            return buttonsData;
        }


    }

    public struct KeyData
    {
        public string Key { get; set; }
        public string Usage { get; set; }
    }


    public static class AutomationExtensions
    {
        public static string GetText(this AutomationElement element)
        {
            object patternObj;
            if (element.TryGetCurrentPattern(ValuePattern.Pattern, out patternObj))
            {
                var valuePattern = (ValuePattern)patternObj;
                return valuePattern.Current.Value;
            }
            else if (element.TryGetCurrentPattern(TextPattern.Pattern, out patternObj))
            {
                var textPattern = (TextPattern)patternObj;
                return textPattern.DocumentRange.GetText(-1).TrimEnd('\r'); // often there is an extra '\r' hanging off the end.
            }
            else
            {
                return element.Current.Name;
            }
        }

        public static AutomationElementCollection GetElements(this AutomationElement element, ControlType controlType)
        {
            PropertyCondition condType = new PropertyCondition(AutomationElement.ControlTypeProperty, controlType);
            return element.FindAll(TreeScope.Descendants, condType);
        }
        
        public static AutomationElement FindChildByProcessId(this AutomationElement element, int processId)
        {
            var result = element.FindChildByCondition(
                new PropertyCondition(AutomationElement.ProcessIdProperty, processId));

            return result;
        }

        
        public static AutomationElement GetElementByClassName(this AutomationElement element, string name, string className)
        {
            PropertyCondition condName = new PropertyCondition(AutomationElement.NameProperty, name);
            PropertyCondition condClassName = new PropertyCondition(AutomationElement.ClassNameProperty, className);
            AndCondition cond = new AndCondition(condName, condClassName);
            return element.FindFirst(TreeScope.Descendants, cond);
        }

        public static AutomationElement FindChildByCondition(this AutomationElement element, Condition condition)
        {
            var result = element.FindFirst(
                TreeScope.Children,
                condition);

            return result;
        }
    }



}
